﻿namespace JetShop.CodeChallenge.UnitTests
{
    using System;
    using System.Collections.Generic;

    using NUnit.Framework;

    public class FreightCalculatorTests
    {
        private List<FreightService> DefaultFreightServices = new List<FreightService>
        {
            new FreightService
            {
                Id = 1,
                FreeFreightLimit = null,
                Type = FreightType.Fixed,
                FixedFreightCost = 100
            },
            new FreightService
            {
                Id = 2,
                FreeFreightLimit = null,
                Type = FreightType.Weight,
                CostsPerWeightUnit = new Dictionary<int, decimal>
                {
                    {10, 10},
                    {35, 5},
                    {40, 3},
                    {43, 1}
                }
            },
            new FreightService
            {
                Id = 3,
                FreeFreightLimit = 15000,
                Type = FreightType.Fixed,
                FixedFreightCost = 10000
            },
            new FreightService
            {
                Id = 4,
                FreeFreightLimit = 10000,
                Type = FreightType.Weight,
                CostsPerWeightUnit = new Dictionary<int, decimal>
                {
                    {0, 100},
                    {50, 50}
                }
            }
        };

        [TestCase(1, 100)]
        [TestCase(2, 160)]
        [TestCase(3, 10000)]
        [TestCase(4, 1600)]
        public void CalculateFreightCost_WhenProductsNeverReachFreightLimit(int freightServiceId, decimal expectedFreightCost)
        {
            // Arrange
            var products = new List<Product>
            {
                new Product { Price = 100, Weight = 1 },
                new Product { Price = 1000, Weight = 1 },
                new Product { Price = 170, Weight = 5 },
                new Product { Price = 350, Weight = 1 },
                new Product { Price = 114, Weight = 3 },
                new Product { Price = 1123, Weight = 4 },
                new Product { Price = 111.14M, Weight = 1 }
            };

            var freightCalculator = new FreightCalculator(this.DefaultFreightServices);

            // Act
            var freightCost = freightCalculator.CalculateFreightCost(products, freightServiceId);

            // Assert
            Assert.That(freightCost, Is.EqualTo(expectedFreightCost));
        }

        [TestCase(3, 0)]
        [TestCase(4, 0)]
        public void CalculateFreightCost_WhenProductsReachFreightLimit(int freightServiceId, decimal expectedFreightCost)
        {
            // Arrange
            var products = new List<Product>
            {
                new Product { Price = 100, Weight = 1 },
                new Product { Price = 1000, Weight = 1 },
                new Product { Price = 1700, Weight = 5 },
                new Product { Price = 3500, Weight = 1 },
                new Product { Price = 1140, Weight = 3 },
                new Product { Price = 11230, Weight = 4 },
                new Product { Price = 111, Weight = 1 }
            };

            var freightCalculator = new FreightCalculator(this.DefaultFreightServices);

            // Act
            var freightCost = freightCalculator.CalculateFreightCost(products, freightServiceId);

            // Assert
            Assert.That(freightCost, Is.EqualTo(expectedFreightCost));
        }

        [Test]
        public void CalculateFreightCost_WhenNoFreightServiceFound_ThrowsException()
        {
            // Arrange
            const int NonExistingFreightServiceId = 1337;
            var freightCalculator = new FreightCalculator(this.DefaultFreightServices);

            // Act && Assert
            var exception = Assert.Throws<Exception>(() => freightCalculator.CalculateFreightCost(null, NonExistingFreightServiceId));

            Assert.That(exception.Message, Is.EqualTo("FreightService not found."));
        }

        [Test]
        public void CalculateFreightCost_WhenTwoFreightServicesHaveSameId_ThrowsException()
        {
            // Arrange
            var duplicatedFreightServices = new List<FreightService> { new FreightService { Id = 1 }, new FreightService { Id = 1 } };
            var freightCalculator = new FreightCalculator(duplicatedFreightServices);

            // Act && Assert
            var exception = Assert.Throws<InvalidOperationException>(() => freightCalculator.CalculateFreightCost(null, 1));

            Assert.That(exception.Message, Is.EqualTo("Sequence contains more than one element"));
        }

        [Test]
        public void CalculateFreightCost_WhenProductsIsNull_ReturnsZeroFreightCost()
        {
            // Arrange
            var freightCalculator = new FreightCalculator(this.DefaultFreightServices);

            // Act
            var freightCost = freightCalculator.CalculateFreightCost(null, 1);

            // Assert
            Assert.That(freightCost, Is.EqualTo(0));
        }
    }
}