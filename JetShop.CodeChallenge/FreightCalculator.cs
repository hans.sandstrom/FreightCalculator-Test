﻿namespace JetShop.CodeChallenge
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class FreightCalculator
    {
        private List<FreightService> FreightServices;

        public FreightCalculator(List<FreightService> freightServices)
        {
            this.FreightServices = freightServices;
        }

        public decimal CalculateFreightCost(IEnumerable<Product> products, int freightServiceId)
        {
            decimal freightCost;
            var freightService = FreightServices.Where(service => service.Id == freightServiceId).First();

            if (freightService.Type == FreightType.Fixed)
            {
                if (freightService.FreeFreightLimit.HasValue)
                {
                    throw new NotImplementedException();
                }
                else
                {
                    freightCost = freightService.FixedFreightCost;
                }
            }
            else
            {
                if (freightService.FreeFreightLimit.HasValue)
                {
                    var totalProductSumDecimal = products.Sum(prod => prod.Price);
                    if (totalProductSumDecimal >= freightService.FreeFreightLimit)
                    {
                        freightCost = 0;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }
                else
                {
                    var totalProductWeight = products.Sum(product => product.Weight);

                    KeyValuePair<int, decimal> foundCPWUKVP;
                    foreach (var cPWUKVP in freightService.CostsPerWeightUnit)
                    {
                        if (cPWUKVP.Key <= totalProductWeight)
                        {
                            if (foundCPWUKVP != null)
                            {
                                var currentDiff = foundCPWUKVP.Key - totalProductWeight;
                                var newDiff = cPWUKVP.Key - totalProductWeight;

                                if (currentDiff > newDiff)
                                {
                                    foundCPWUKVP = cPWUKVP;
                                }
                            }
                            else
                            {
                                foundCPWUKVP = cPWUKVP;
                            }
                        }
                    }

                    if (foundCPWUKVP == null)
                    {
                        foundCPWUKVP = freightService.CostsPerWeightUnit.First();
                    }

                    freightCost = foundCPWUKVP.Value * totalProductWeight;
                }
            }

            return freightCost;
        }
    }
}