﻿namespace JetShop.CodeChallenge
{
    using System.Collections.Generic;

    public class FreightService
    {
        public int Id { get; set; }

        /// <summary>
        /// If not null then, when cart total cost is above or equal to this, freight is free.
        /// </summary>
        public decimal? FreeFreightLimit { get; set; }

        /// <summary>
        /// Weight then <see cref="CostsPerWeightUnit"/> should be used.
        /// Fixed then <see cref="FixedFreightCost"/> should be use.
        /// </summary>
        public FreightType Type { get; set; }

        /// <summary>
        /// Value defines the cost per weight unit. The current cost is determined by the current total weight.
        /// A valid key for for total weights is lower and/or equal to the key. This must also be the key closest to the total weight.
        /// If none lower than or equal to total weight is found. The lowest one is used.
        /// </summary>
        public Dictionary<int, decimal> CostsPerWeightUnit { get; set; }

        public decimal FixedFreightCost { get; set; }
    }
}