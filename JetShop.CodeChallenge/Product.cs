﻿namespace JetShop.CodeChallenge
{
    public class Product
    {
        public int Weight { get; set; }
        public decimal Price { get; set; }
    }
}